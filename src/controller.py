from src.image import Image
import src.utils as utils
from src.model import Model


populate_hist_mode = True
hist_start = 15100097
img_dir = "E:\\studia\\praca inzynierska\\faceID\\data\\img\\me2.jpg"
feret_dir = 'E:\\studia\\praca inzynierska\\faceID\\data\\feret_img'
predictor_path = 'E:\\studia\\praca inzynierska\\faceID\\data\\model\\shape_predictor_68_face_landmarks.dat'

if populate_hist_mode:
    utils.populate_histograms('E:\\studia\\praca inzynierska\\faceID\\data\\feret_img', hist_start)

image_object = Image(img_dir, predictor_path, resize_width=500)
print(image_object.image_path)

model = Model(feret_dir)
model.intersection_test()
