from matplotlib import pyplot as plt
import os
from src.image import Image
import pickle


def show_output(display_list):
    output_list_len = len(display_list)
    figure = plt.figure()
    for i in range(output_list_len):
        current_dict = display_list[i]
        current_img = current_dict["img"]
        current_xlabel = current_dict["xlabel"]
        current_ylabel = current_dict["ylabel"]
        current_xtick = current_dict["xtick"]
        current_ytick = current_dict["ytick"]
        current_title = current_dict["title"]
        current_type = current_dict["type"]
        current_plot = figure.add_subplot(1, output_list_len, i + 1)
        if current_type == "gray":
            current_plot.imshow(current_img, cmap=plt.get_cmap('gray'))
            current_plot.set_title(current_title)
            current_plot.set_xticks(current_xtick)
            current_plot.set_yticks(current_ytick)
            current_plot.set_xlabel(current_xlabel)
            current_plot.set_ylabel(current_ylabel)
        elif current_type == "histogram":
            current_plot.plot(current_img, color="black")
            current_plot.set_xlim([0, 57])
            current_plot.set_title(current_title)
            current_plot.set_xlabel(current_xlabel)
            current_plot.set_ylabel(current_ylabel)
            ytick_list = [int(i) for i in current_plot.get_yticks()]
            current_plot.set_yticklabels(ytick_list, rotation=90)
    plt.show()


def populate_histograms(directory, starting_point):
    for root, dirs, files in os.walk(directory):
        for filename in files:
            if not os.path.isdir(directory + "\\" + filename.split('_')[0]):
                os.mkdir(directory + "\\" + filename.split('_')[0])
            if os.path.exists(directory + "\\" + filename):
                os.rename(directory + "\\" + filename, directory + "\\" + filename.split('_')[0] + "\\" + filename)
        for dir in dirs:
            print(dir)
            with open("E:\\studia\\praca inzynierska\\faceID\\data\\populate_log", "w") as log_file:
                log_file.write(dir)
            log_file.close()
            if int(dir) > starting_point:
                for dirs in os.walk(directory + "\\" + dir):
                    for images in os.walk(dirs[0]):
                        for i in range(len(dirs[2])):
                            print(dirs[0] + "\\" + dirs[2][i])
                            if not dirs[2][i].split('_')[-1] == 'histogram':
                                image_object = Image(dirs[0] + "\\" + dirs[2][i], '', resize_width=1000)
                                image_object.lbp_histogram()
                                lbp_hist = image_object.img_lbp_histogram
                                uniform_hist = image_object.img_uniform_lbp_histogram
                                with open(dirs[0] + "\\" + dirs[2][i] + "_lbp_histogram", 'wb') as fp:
                                    pickle.dump(lbp_hist, fp)
                                fp.close()
                                with open(dirs[0] + "\\" + dirs[2][i] + "_lbp_uniform_histogram", 'wb') as fp:
                                    pickle.dump(uniform_hist, fp)
                                fp.close()
