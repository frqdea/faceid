import os
import math
import random
import pickle
import cv2


class Model:
    feret_dir = ''
    training_images = []
    test_images = []
    validation_images = []
    histograms = []

    def __init__(self, feret_dir, training_test_validation=[70, 15, 15]):
        self.feret_dir = feret_dir
        if sum(training_test_validation) != 100:
            training_test_validation = [70, 15, 15]
            print("Invalid sizes of training/test/validation set proportions. Using default values (70, 15, 15).")

        directory_names = []
        for directories, roots, files in os.walk(feret_dir):
            for i in range(0, len(roots)):
                directory_names.append(feret_dir + "\\" + roots[i])

        training_set_size = math.floor(training_test_validation[0]/100 * len(directory_names))
        test_set_size = math.floor(training_test_validation[1]/100 * len(directory_names))
        validation_set_size = math.floor(training_test_validation[2]/100 * len(directory_names))

        training_set_dirs = directory_names[:training_set_size]
        test_set_dirs = directory_names[training_set_size:training_set_size+test_set_size+1]
        validation_set_dirs = directory_names[training_set_size+test_set_size:training_set_size+test_set_size+validation_set_size+2]

        for dir_name in training_set_dirs:
            for directories, roots, files in os.walk(dir_name):
                for filename in files:
                    if not filename.split('_')[-1] == "histogram":
                        self.training_images.append(dir_name + "\\" + filename)

        for dir_name in test_set_dirs:
            for directories, roots, files in os.walk(dir_name):
                for filename in files:
                    if not filename.split('_')[-1] == "histogram":
                        self.test_images.append(dir_name + "\\" + filename)

        for dir_name in validation_set_dirs:
            for directories, roots, files in os.walk(dir_name):
                for filename in files:
                    if not filename.split('_')[-1] == "histogram":
                        self.validation_images.append(dir_name + "\\" + filename)

        for dir_name in validation_set_dirs + test_set_dirs + training_set_dirs:
            for directories, roots, files in os.walk(dir_name):
                for filename in files:
                    if filename.split('_')[-1] == "histogram" and filename.split('_')[-2] == "uniform":
                        if os.path.getsize(dir_name + "\\" + filename) > 0:
                            with open(dir_name + "\\" + filename, "rb") as test_histogram_file:
                                hist = pickle.load(test_histogram_file)
                            if len(hist) == 4350:
                                self.histograms.append(dir_name + "\\" + filename)

    def chi_square_test(self):
        random.shuffle(self.histograms)
        test_histograms = self.histograms[:80]
        print("Using " + str(len(test_histograms)) + " random images as validation set")
        del self.histograms[:80]
        print("Database contains " + str(len(self.histograms)) + " images in total")

        front_count = 0
        quarter_count = 0
        profile_count = 0
        half_count = 0
        r15_count = 0
        r4575_count = 0
        total_front_count = 0
        total_quarter_count = 0
        total_profile_count = 0
        total_half_count = 0
        total_r15_count = 0
        total_r4575_count = 0
        else_count = 0

        for histogram in test_histograms:
            currest_best_chi_square = 999
            best_chi_square_filename = ''
            with open(histogram, 'rb') as histogram_file:
                current_histogram = pickle.load(histogram_file)
            for test_histogram in self.histograms:
                with open(test_histogram, "rb") as test_histogram_file:
                    current_test_histogram = pickle.load(test_histogram_file)
                chi_square = 0.0
                if len(current_test_histogram) == len(current_histogram):
                    for i in range(len(current_histogram)):
                        chi_square += (pow(current_histogram[i]-current_test_histogram[i], 2)
                                       /(current_histogram[i]+current_test_histogram[i]))
                if chi_square < currest_best_chi_square:
                    currest_best_chi_square = chi_square
                    best_chi_square_filename = test_histogram
            print(str(round(currest_best_chi_square * 100 / 75, 1)) + "% confidence")
            if best_chi_square_filename != '' and best_chi_square_filename.split('\\')[6] \
                    == histogram.split('\\')[6]:
                if "f" in histogram.split("_")[3]:
                    front_count += 1
                elif "q" in histogram.split("_")[3]:
                    quarter_count += 1
                elif "p" in histogram.split("_")[3]:
                    profile_count += 1
                elif "h" in histogram.split("_")[3]:
                    half_count += 1
                elif (histogram.split("_")[3] == "ra"
                      or histogram.split("_")[3] == "rd"
                      or histogram.split("_")[3] == "re"):
                    r4575_count += 1
                elif (histogram.split("_")[3] == "rb"
                      or histogram.split("_")[3] == "rc"):
                    r15_count += 1
            if "f" in histogram.split("_")[3]:
                total_front_count += 1
            elif "q" in histogram.split("_")[3]:
                total_quarter_count += 1
            elif "p" in histogram.split("_")[3]:
                total_profile_count += 1
            elif "h" in histogram.split("_")[3]:
                total_half_count += 1
            elif (histogram.split("_")[3] == "ra"
                  or histogram.split("_")[3] == "rd"
                  or histogram.split("_")[3] == "re"):
                total_r4575_count += 1
            elif (histogram.split("_")[3] == "rb"
                  or histogram.split("_")[3] == "rc"):
                total_r15_count += 1
            else:
                else_count += 1

            print(str(front_count) + "/" + str(total_front_count) + " hit rate for front:"
                  + str(front_count / max(1, total_front_count) * 100))
            print(str(quarter_count) + "/" + str(total_quarter_count) + " hit rate for quarter:"
                  + str(quarter_count / max(1, total_quarter_count) * 100))
            print(str(half_count) + "/" + str(total_half_count) + " hit rate for half:"
                  + str(half_count / max(1, total_half_count) * 100))
            print(str(profile_count) + "/" + str(total_profile_count) + " hit rate for profile:"
                  + str(profile_count / max(1, total_profile_count) * 100))
            print(str(r15_count) + "/" + str(total_r15_count) + " hit rate for 15 degrees:"
                  + str(r15_count / max(1, total_r15_count) * 100))
            print(str(r4575_count) + "/" + str(total_r4575_count) + " hit rate for 45-75 degrees:"
                  + str(r4575_count / max(1, total_r4575_count) * 100))
            print(str(else_count) + " else count")

    def intersection_test(self):
        random.shuffle(self.histograms)
        test_histograms = self.histograms[:80]
        print("Using " + str(len(test_histograms)) + " random images as validation set")
        del self.histograms[:80]
        print("Database contains " + str(len(self.histograms)) + " images in total")

        front_count = 0
        quarter_count = 0
        profile_count = 0
        half_count = 0
        r15_count = 0
        r4575_count = 0
        total_front_count = 0
        total_quarter_count = 0
        total_profile_count = 0
        total_half_count = 0
        total_r15_count = 0
        total_r4575_count = 0
        else_count = 0

        for histogram in test_histograms:
            current_best_intersection = 0
            best_intersection_filename = ''
            with open(histogram, 'rb') as histogram_file:
                current_histogram = pickle.load(histogram_file)
            for test_histogram in self.histograms:
                with open(test_histogram, "rb") as test_histogram_file:
                    current_test_histogram = pickle.load(test_histogram_file)
                intersection = 0.0
                if len(current_test_histogram) == len(current_histogram):
                    for i in range(len(current_histogram)):
                        intersection += float(min(current_test_histogram[i], current_histogram[i]))
                if intersection > current_best_intersection:
                    current_best_intersection = intersection
                    best_intersection_filename = test_histogram
            print(str(round(current_best_intersection * 100 / 75, 1)) + "% confidence")
            if best_intersection_filename != '' and best_intersection_filename.split('\\')[6] \
                    == histogram.split('\\')[6]:
                if "f" in histogram.split("_")[3]:
                    front_count += 1
                elif "q" in histogram.split("_")[3]:
                    quarter_count += 1
                elif "p" in histogram.split("_")[3]:
                    profile_count += 1
                elif "h" in histogram.split("_")[3]:
                    half_count += 1
                elif (histogram.split("_")[3] == "ra"
                        or histogram.split("_")[3] == "rd"
                        or histogram.split("_")[3] == "re"):
                    r4575_count += 1
                elif (histogram.split("_")[3] == "rb"
                        or histogram.split("_")[3] == "rc"):
                    r15_count += 1
            if "f" in histogram.split("_")[3]:
                total_front_count += 1
            elif "q" in histogram.split("_")[3]:
                total_quarter_count += 1
            elif "p" in histogram.split("_")[3]:
                total_profile_count += 1
            elif "h" in histogram.split("_")[3]:
                total_half_count += 1
            elif (histogram.split("_")[3] == "ra"
                  or histogram.split("_")[3] == "rd"
                  or histogram.split("_")[3] == "re"):
                total_r4575_count += 1
            elif (histogram.split("_")[3] == "rb"
                  or histogram.split("_")[3] == "rc"):
                total_r15_count += 1
            else:
                else_count += 1

            print(str(front_count) + "/" + str(total_front_count) + " hit rate for front:"
                  + str(front_count/max(1, total_front_count)*100))
            print(str(quarter_count) + "/" + str(total_quarter_count) + " hit rate for quarter:"
                  + str(quarter_count/max(1, total_quarter_count)*100))
            print(str(half_count) + "/" + str(total_half_count) + " hit rate for half:"
                  + str(half_count/max(1, total_half_count)*100))
            print(str(profile_count) + "/" + str(total_profile_count) + " hit rate for profile:"
                  + str(profile_count/max(1, total_profile_count)*100))
            print(str(r15_count) + "/" + str(total_r15_count) + " hit rate for 15 degrees:"
                  + str(r15_count/max(1, total_r15_count)*100))
            print(str(r4575_count) + "/" + str(total_r4575_count) + " hit rate for 45-75 degrees:"
                  + str(r4575_count/max(1, total_r4575_count)*100))
            print(str(else_count) + " else count")

    def least_squares_test(self):
        random.shuffle(self.histograms)
        test_histograms = self.histograms[:800]
        print(len(test_histograms))
        print(len(self.histograms))
        del self.histograms[:800]

        count = 0
        total_count = 0
        for histogram in test_histograms:
            current_lowest_squares_sum = 9999999999999999999999999999999999
            lowest_square_sum_filename = ''
            with open(histogram, 'rb') as histogram_file:
                current_histogram = pickle.load(histogram_file)
            for test_histogram in self.histograms:
                with open(test_histogram, "rb") as test_histogram_file:
                    current_test_histogram = pickle.load(test_histogram_file)
                squares_sum = 0.0
                if len(current_test_histogram) == len(current_histogram):
                    for i in range(len(current_histogram)):
                        squares_sum += math.sqrt((current_histogram[i] - current_test_histogram[i])
                                                 * (current_histogram[i] - current_test_histogram[i]))
                if squares_sum < current_lowest_squares_sum:
                    current_lowest_squares_sum = squares_sum
                    lowest_square_sum_filename = test_histogram
            if (lowest_square_sum_filename != '' and lowest_square_sum_filename.split('\\')[6]
                    == histogram.split('\\')[6]):
                print(current_lowest_squares_sum)
                count += 1
            total_count += 1
            print(str(round(count/total_count * 100, 2)) + "% success rate")

    def intersection_id(self, image):
        image.lbp_histogram()
        uniform_lbp_hist = image.img_uniform_lbp_histogram
        print(uniform_lbp_hist)
        print(len(uniform_lbp_hist))
        current_best_intersection = 0
        best_intersection_filename = ''

        for test_histogram in self.histograms:
            with open(test_histogram, "rb") as test_histogram_file:
                current_test_histogram = pickle.load(test_histogram_file)
            intersection = 0.0
            if len(current_test_histogram) == len(uniform_lbp_hist):
                for i in range(len(uniform_lbp_hist)):
                    intersection += float(min(current_test_histogram[i], uniform_lbp_hist[i]))
            if 24.999 > intersection > current_best_intersection:
                current_best_intersection = intersection
                best_intersection_filename = test_histogram
        print(str(round(current_best_intersection*100/25, 1)) + "% confidence")
        for i in range(3, len(best_intersection_filename)):
            if best_intersection_filename[i-3:i] == "ppm":
                best_intersection_filename = best_intersection_filename[0:i]

        print(best_intersection_filename)
        cv2.imshow("db image", cv2.imread(best_intersection_filename))
        image.lbp_histogram()
        cv2.imshow("input image", cv2.imread(image.image_path))
        cv2.waitKey(0)
