import numpy as np
import dlib
import cv2
import imutils
from imutils import face_utils
import time
import math


class Image:
    image_path = ''
    shape = np.zeros(68)
    bounding_boxes = []
    predictor_path = ''
    img_lbp_histogram = []
    img_uniform_lbp_histogram = []

    def __init__(self, image_path, predictor_path, resize_width):
        self.image_path = image_path
        self.predictor_path = predictor_path
        self.resize_width = resize_width

    def find_landmarks(self):
        start = time.time()
        shape_predictor = dlib.shape_predictor(self.predictor_path)
        detector = dlib.get_frontal_face_detector()
        image = cv2.imread(self.image_path)
        image = imutils.resize(image, width=self.resize_width)
        image_greyscale = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        face_bounding_boxes = detector(image_greyscale, 1)
        shape = []

        for (i, face_bounding_box) in enumerate(face_bounding_boxes):
            shape = shape_predictor(image_greyscale, face_bounding_box)
            shape = face_utils.shape_to_np(shape)

        self.bounding_boxes = face_bounding_boxes
        self.shape = shape
        print(shape)
        end = time.time()
        print("landmarking time: " + str(round(end-start, 2)))

    def display_landmarks(self):
        image = cv2.imread(self.image_path)
        image = imutils.resize(image, width=self.resize_width)
        h = image.shape[0]
        w = image.shape[1]
        blank_image = image

        for (i, face_bounding_box) in enumerate(self.bounding_boxes):
            (x, y, w, h) = face_utils.rect_to_bb(face_bounding_box)
            cv2.rectangle(blank_image, (x, y), (x + w, y + h), (0, 255, 0), 2)
            cv2.putText(blank_image, "Face #{}".format(i + 1), (x - 10, y - 10),
                        cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 2)
            for (x, y) in self.shape:
                cv2.circle(blank_image, (x, y), 1, (0, 0, 255), -1)

        cv2.imshow("Face Landmarks", blank_image)
        cv2.waitKey(0)

    def lbp_histogram(self, neighbourhoods=[[8, 2], [12, 3], [16, 4]], no_of_masks=25):
        uniform_lbp = [0, 1, 2, 3, 4, 58, 5, 6, 7, 58, 58, 58, 8, 58, 9, 10, 11, 58, 58, 58, 58, 58,
                       58, 58, 12, 58, 58, 58, 13, 58, 14, 15, 16, 58, 58, 58, 58, 58, 58, 58, 58, 58,
                       58, 58, 58, 58, 58, 58, 17, 58, 58, 58, 58, 58, 58, 58, 18, 58, 58, 58, 19, 58,
                       20, 21, 22, 58, 58, 58, 58, 58, 58, 58, 58, 58, 58, 58, 58, 58, 58, 58, 58, 58,
                       58, 58, 58, 58, 58, 58, 58, 58, 58, 58, 58, 58, 58, 58, 23, 58, 58, 58, 58, 58,
                       58, 58, 58, 58, 58, 58, 58, 58, 58, 58, 24, 58, 58, 58, 58, 58, 58, 58, 25, 58,
                       58, 58, 26, 58, 27, 28, 29, 30, 58, 31, 58, 58, 58, 32, 58, 58, 58, 58, 58, 58,
                       58, 33, 58, 58, 58, 58, 58, 58, 58, 58, 58, 58, 58, 58, 58, 58, 58, 34, 58, 58,
                       58, 58, 58, 58, 58, 58, 58, 58, 58, 58, 58, 58, 58, 58, 58, 58, 58, 58, 58, 58,
                       58, 58, 58, 58, 58, 58, 58, 58, 58, 35, 36, 37, 58, 38, 58, 58, 58, 39, 58, 58,
                       58, 58, 58, 58, 58, 40, 58, 58, 58, 58, 58, 58, 58, 58, 58, 58, 58, 58, 58, 58,
                       58, 41, 42, 43, 58, 44, 58, 58, 58, 45, 58, 58, 58, 58, 58, 58, 58, 46, 47, 48,
                       58, 49, 58, 58, 58, 50, 51, 52, 58, 53, 54, 55, 56, 57]

        detector = dlib.get_frontal_face_detector()
        image = cv2.imread(self.image_path)
        image_greyscale = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        image_greyscale = imutils.resize(image_greyscale, 1000)
        face_bounding_boxes = detector(image_greyscale, 1)
        face_images = []

        for face_bounding_box in face_bounding_boxes:
            (x, y, w, h) = face_utils.rect_to_bb(face_bounding_box)
            face_images.append(image_greyscale[y:y+h, x:x+w])

        lbp_histogram, concatenated_lbp_histograms = [], []

        for neighbourhood in neighbourhoods:
            print(neighbourhood)
            no_of_pixels = neighbourhood[0]
            radius = neighbourhood[1]
            uniform_lbp_histograms = []
            for face_image in face_images:
                transformed_face_image = self.lbp_transform(face_image, no_of_pixels, radius)
                uniform_transformed_face_image = np.zeros((transformed_face_image.shape[0],
                                                           transformed_face_image.shape[1],
                                                           3), np.uint8)
                lbp_histogram = cv2.calcHist([transformed_face_image], [0], None, [256], [0, 256])
                for i in range(len(uniform_transformed_face_image)):
                    for j in range(len(uniform_transformed_face_image[i])):
                        uniform_transformed_face_image[i][j] = uniform_lbp[transformed_face_image[i][j][0]]
                mask_width = uniform_transformed_face_image.shape[0]
                mask_height = uniform_transformed_face_image.shape[1]
                no_of_mask_rows_cols = int(math.sqrt(no_of_masks))

                if no_of_mask_rows_cols < 1:
                    print("Incorrect number of regions for lbp histogram")
                    return

                masks = [[np.zeros((mask_width, mask_height), np.uint8)
                         for i in range(no_of_mask_rows_cols)]
                         for j in range(no_of_mask_rows_cols)]

                for i in range(no_of_mask_rows_cols):
                    for j in range(no_of_mask_rows_cols):
                        for k in range(int(i*mask_width/no_of_mask_rows_cols),
                                       int((i+1)*mask_width/no_of_mask_rows_cols)):
                            for l in range(int(j*mask_height/no_of_mask_rows_cols),
                                           int((j+1)*mask_height/no_of_mask_rows_cols)):
                                masks[i][j][k][l] = 255

                uniform_lbp_histograms_by_mask = []

                for mask_row in masks:
                    for mask in mask_row:
                        hist = cv2.calcHist([uniform_transformed_face_image], [0], mask, [58], [0, 57]).astype("float")
                        hist /= (hist.sum())
                        uniform_lbp_histograms_by_mask.append(hist)
                if type(uniform_lbp_histograms) != type(np.zeros((1, 1))):
                    uniform_lbp_histograms.append(np.concatenate(uniform_lbp_histograms_by_mask))
                    uniform_lbp_histograms = np.concatenate(uniform_lbp_histograms)
            concatenated_lbp_histograms.append(uniform_lbp_histograms)
        concatenated_lbp_histograms = np.concatenate(concatenated_lbp_histograms)
        print(concatenated_lbp_histograms.shape)
        #cv2.imshow("img", uniform_transformed_face_image)
        #cv2.waitKey(0)
        uniform_transformed_face_image, self.img_uniform_lbp_histogram = lbp_histogram, concatenated_lbp_histograms

    @staticmethod
    def get_pixel(img, center_value, x, y):
        new_value = '0'
        try:
            if img[x][y] >= center_value:
                new_value = '1'
        except IndexError:
            pass
        return new_value

    def lbp_transform(self, face_image, no_of_pixels, radius):
        width = face_image.shape[0]
        height = face_image.shape[1]
        lbp_transformed_image = np.zeros((width, height, 3), np.uint8)

        for x in range(1, width):
            for y in range(1, height):
                center_value = face_image[x][y]
                binary_center_pixel_value = ''
                binary_center_pixel_value += (self.get_pixel(face_image, center_value, x - 1, y + 1))
                binary_center_pixel_value += (self.get_pixel(face_image, center_value, x, y + 1))
                binary_center_pixel_value += (self.get_pixel(face_image, center_value, x + 1, y + 1))
                binary_center_pixel_value += (self.get_pixel(face_image, center_value, x + 1, y))
                binary_center_pixel_value += (self.get_pixel(face_image, center_value, x + 1, y - 1))
                binary_center_pixel_value += (self.get_pixel(face_image, center_value, x, y - 1))
                binary_center_pixel_value += (self.get_pixel(face_image, center_value, x - 1, y - 1))
                binary_center_pixel_value += (self.get_pixel(face_image, center_value, x - 1, y))
                lbp_transformed_image[x][y] = int(binary_center_pixel_value[::-1], 2)
                lbp_transformed_image[x][y] = self.calculate_binary_increment(face_image,
                                                                              x, y, center_value,
                                                                              no_of_pixels, radius)
        return lbp_transformed_image

    def calculate_binary_increment(self, face_image, xc, yc, center_value, no_of_pixels, radius):
        binary_center_pixel_value = ''

        for pixel in range(no_of_pixels):
            xp = xc + radius * math.sin(2*math.pi*pixel / no_of_pixels)
            yp = yc + radius * math.cos(2*math.pi*pixel / no_of_pixels)
            lbp_value = '0'

            try:
                x1 = math.floor(xp)
                x2 = math.ceil(xp)
                y1 = math.ceil(yp)
                y2 = math.floor(yp)
                q11 = face_image[x1][y1]
                q12 = face_image[x1][y2]
                q21 = face_image[x2][y1]
                q22 = face_image[x2][y2]

                if q11 == q12 == q21 == q22:
                    bilinear_interp_pixel_value = q11
                else:
                    bilinear_interp_pixel_value = round(((x2-xp) * (y2-yp) * q11
                                                        + (xp-x1) * (y2-yp) * q21
                                                        + (x2-xp) * (yp-y1) * q12
                                                        + (xp-x1) * (yp-y1) * q22)
                                                        / ((x2-x1) * (y2-y1) + 0.0))

                lbp_value = '0'
                if bilinear_interp_pixel_value >= center_value:
                    lbp_value = '1'
            except IndexError:
                pass
            binary_center_pixel_value += lbp_value

        return int(binary_center_pixel_value[::-1], 2)
